global exit
global string_length
global print_string
global print_error ;new one
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

;дальше бога нет
section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        mov rsi, [rdi]
        and rsi, 0xff
        test rsi, rsi
        jz .exit

        inc rax
        inc rdi
        jmp .loop
    .exit:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1

    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdx, 1
    mov rdi, 1

    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 0
    xor r8, r8
    mov r8, 10
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
    inc rcx
    .loop:
        xor rdx, rdx
        inc rcx
        div r8
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .print_uint


    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .print_uint:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r9b, byte[rdi]
        cmp r9b, byte[rsi]
        jne .null
        cmp r9b, 0
        je .ein
        inc rdi
        inc rsi
        jmp .loop
    .null:
        mov rax, 0
        ret
    .ein:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    push 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1

    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13
    push r14
    push r15
    mov r13, rdi
    mov r14, rdi
    mov r15, rsi

    .skip_prob:
        call read_char
        cmp al, 0x20
        je .skip_prob
        cmp al, 0xA
        je .skip_prob
        cmp al, 0x9
        je .skip_prob

    .read:
        test rax,rax
        je .fine
        cmp al, 0x20
        je .fine
        cmp al, 0xA
        je .fine
        cmp al, 0x9
        je .fine

        dec r15
        test r15, r15
        jz .notfine
        mov byte[r14], al
        inc r14
        call read_char
        jmp .read

    .fine:
        mov byte[r14],0
        sub r14, r13
        mov rdx, r14
        mov rax, r13
        jmp .exit
    .notfine:
        xor rax, rax
        jmp .exit

    .exit:
        pop r15
        pop r14
        pop r13
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    .loop:
        mov al, [rdi+rcx]
        cmp al, 0
        jz .exit
        sub al, "0"
        js .exit
        cmp al, 9
        ja .exit
        inc rcx
        imul rdx, 10
        add dl, al
        jmp .loop
    .exit:
        mov rax, rdx
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, [rdi]
    cmp al, "-"
    je .neg
    call parse_uint
    ret

    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r9, r9
    .loop:
        cmp rax, rdx
        jge .fail
        mov cl, [rdi+rax]
        mov [rsi+rax], cl
        cmp cl,0
        je .fine
        inc rax
        jmp .loop
    .fail:
        xor rax, rax
    .fine:
        ret
;thanksforwatching, Denis K. Berman
