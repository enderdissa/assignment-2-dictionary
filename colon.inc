%define head 0
%macro colon 2
    %ifid %2
        %ifstr %1
            %2:
                dq head
                db %1, 0
            %define head %2
        %else
            %error "wrong key"
        %endif
    %else
        %error "wrong label"
    %endif
%endmacro
