%include "lib.inc"
%include "dict.inc"
%include "words.inc"

section .rodata
input_err: db "Input ERROR", 0
not_found: db "Key found ERROR", 0
%define buffer 256
%define keysize 8

section .bss
buff: resb buffer

section .text
global _start

_start:
    mov rdi, buff
    mov rsi, buffer
    call read_word
    test rax, rax
    jz .inp_err
    push rdx
    mov rdi, rax
    mov rsi, head
    call find_word
    pop rdx
    test rax, rax
    jz .not_found_err
    add rax, keysize
    add rax, rdx
    inc rax
    mov rdi, rax
    call print_string
    .fin:
        xor rdi, rdi
        call exit
    .inp_err:
        mov rdi, input_err
        jmp .error
    .not_found_err
        mov rdi, not_found
    .error:
        call print_string
        jmp .fin

