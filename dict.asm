%include "lib.inc"

section .rodata
%define keysize 8


section .text
global find_word

find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi

    .loop:
        mov rdi, r12
        mov rsi, r13
        add rsi, keysize
        call string_equals
        test rax, rax
        jnz .succes
        mov r13, [r13] ;next element
        test r13, r13
        jnz  .loop

    .fail:
        xor rax, rax
        jmp .exit

    .succes:
        mov rax, r13

    .exit:
        pop r13
        pop r12
        ret

