import subprocess
inp = ["", "HappyHalloweeeeeeen", "second", "KUSDVSKvhUKHSfuvoysOSUVSDVScvUSHjifisdoszvVVHLSDVUhsldvkcsvudixcsddadasdasdsdasd"]
outs = ["", "", "second word explanation", ""]
errors = ["Key found ERROR", "Key found ERROR", "", "Key found ERROR", "Input ERROR"]
test_results = ""
for i in range(len(inp)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=inp[i].encode())
    stdout = stdout.decode()
    stderr = stderr.decode()
    if stdout == outs[i] and stderr == errors[i]:
        test_results += "A"
        print("Test +"+str(i+1)+" passed")
    else:
        test_results += "F"
        print(stdout, stderr, "Expected:",outs[i], errors[i])
print(test_results)
